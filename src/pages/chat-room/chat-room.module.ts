import { NgModule, ModuleWithProviders  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatRoomPage } from './chat-room';
import { ChatBotServiceProvider } from '../../providers/chat-bot-service/chat-bot-service';
import { ChatRoomServiceProvider } from '../../providers/chat-room-service/chat-room-service';

@NgModule({
  declarations: [
    ChatRoomPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatRoomPage),
  ],
  exports: [
    ChatRoomPage
  ]
})
export class ChatRoomPageModule {
  static forRoot(): ModuleWithProviders {
      return {
        ngModule: ChatRoomPageModule,
        providers: [ChatBotServiceProvider, ChatRoomServiceProvider]
      }
    }
}
