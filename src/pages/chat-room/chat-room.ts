import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ViewController } from 'ionic-angular';
import { ChatRoomServiceProvider } from '../../providers/chat-room-service/chat-room-service';
import { CandidateData } from '../../utils/candidateData';
import { HomePage } from '../home/home';


/**
 * Generated class for the ChatRoomPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
// THIS PAGE IS THE UI LAYER FOR THE CHAT BOT AS CHAT ROOM

@IonicPage()
@Component({
  selector: 'page-chat-room',
  templateUrl: 'chat-room.html'
})
export class ChatRoomPage {
  chatMessages:any[];
  content: any;  
  candidateObject: any;
  sendButtonDisabled:boolean;
  candidateData:CandidateData;

  @ViewChild(Content) ionContent : Content;
  @ViewChild('ionTextArea') chatTextArea;
  constructor(public navCtrl: NavController, public navParams: NavParams, private chatRoomService: ChatRoomServiceProvider, public viewCtrl:ViewController) {
     this.content = "";
    this.chatMessages = [{}];   
    this.candidateObject = [];   
    this.sendButtonDisabled = true;   
  }

  ionViewDidLoad() {    
    console.log('ionViewDidLoad ChatRoomPage');
  }

  onChange(event){
    //console.log(this.content);
    if(this.content==""){
      this.sendButtonDisabled = true;
    }
    else{
      this.sendButtonDisabled = false;
    }
    
  }


  sendMessage(){ //TO SEND MESSAGE FROM TEXT AREA 
 
    let request_msg = this.content;
    this.userRole(this.content); //PERFORM THE USERROLE AND GET THE RESULT BACK TO CALL AI SERVICE 
 
    let typing:string = "typing.....";
    this.botRole(typing);  //PERFORM THE BOT ROLE

    this.chatRoomService.prepareServiceCall(request_msg).subscribe( data => { // WILL CALL THE CHATBOT SERVICE WHICH WILL RESPONSE WITH MESSAGE THAT TO DISPLAY BY BO

        this.chatMessages.pop();
        this.ionContent.scrollToBottom(this.ionContent.scrollHeight+800); //SCROLL THE SCREEN BOTTOM 
        console.log(data)
        
        if(data[1] && data[1] == "end"){          
          setTimeout(() => {
            this.navCtrl.push(HomePage).then(() => {
            const index = this.viewCtrl.index;
            // then we remove it from the navigation stack
            this.navCtrl.remove(index);   
           });    
          },1000);
             

        }
        
        let len = 0;
          while(len < data.length){ 
            if(data[len].speech !=="" ){            
              this.botRole(data[len].speech); //PERFORM THE BOT ROLE
            }                
            len++;          
          }       
      },
      error => console.error("err",error));

    this.content = "";
    this.sendButtonDisabled = true;
    
  }

  userRole(message){
    let userMessageObject = {
      sender: "user",
      msg: message
    };
    this.chatMessages.push(userMessageObject);       
  }  

  botRole(response){
    
    setTimeout(() => {
        this.ionContent.scrollToBottom(this.ionContent.scrollHeight+800); //SCROLL THE SCREEN
    },800);
    
    let botMessageObject = {
      sender: "bot",
      msg: response
    };
    this.chatMessages.push(botMessageObject);
  }

  ionViewWillLeave(){
      this.chatMessages = [{}];
      
  }

}
