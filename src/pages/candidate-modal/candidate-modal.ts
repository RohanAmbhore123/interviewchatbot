import { Component, OnChanges } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController  } from 'ionic-angular';

/**
 * Generated class for the CandidateModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-candidate-modal',
  templateUrl: 'candidate-modal.html',
})
export class CandidateModalPage implements OnChanges{
  candidateDataObject: any = this.navParams.get('data').userData;
  candidateData: Map<string,string>;
  data: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.candidateData = new Map<string,string>();
    this.data = [];
 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CandidateModalPage');
    this.candidateDataObject.forEach((value, key, index) => {
     
      if(index){
        this.data.push(key + " : " + value);
      }
      
    });
   
  }

  ngOnChanges(){
     
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }

}
