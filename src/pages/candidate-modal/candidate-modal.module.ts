import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CandidateModalPage } from './candidate-modal';

@NgModule({
  declarations: [
    CandidateModalPage,
  ],
  imports: [
    IonicPageModule.forChild(CandidateModalPage),
  ],
  exports: [
    CandidateModalPage
  ]
})
export class CandidateModalPageModule {}
