import { Component, OnChanges, OnInit, AfterViewInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController  } from 'ionic-angular';
import { CandidateStorageServiceProvider } from '../../providers/candidate-storage-service/candidate-storage-service';
import { CandidateModalPage } from '../candidate-modal/candidate-modal';

/**
 * Generated class for the CandidateListPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
// THIS PAGE IS USED TO DISPLAY THE LIST OF THE CANDIDATES

@IonicPage()
@Component({
  selector: 'page-candidate-list',
  templateUrl: 'candidate-list.html',
})
export class CandidateListPage implements OnChanges, OnInit, AfterViewInit{
  candidateList:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private candidateStorage : CandidateStorageServiceProvider,public modalCtrl: ModalController, public loading: LoadingController) {
  }

  ionViewDidLoad() {
   
  }

  ngAfterViewInit(){
    
    console.log("candidate list")
    this.candidateStorage.getAllData().subscribe((data) => { // TO FETCH THE DATA FROM DB
      this.candidateList = data;
      console.log(data);
    },
    (err) => {
      console.error("err",err);
    });
  }

  ngOnChanges(){
    console.log("on changes")
  }

  ngOnInit(){
    console.log("oninit")
  }

  openModal(candidateData){  // TO OPEN THE MODAL FOR DETAIL VIEW OF THE LIST
    let obj = {data: candidateData};
    let myModal = this.modalCtrl.create(CandidateModalPage, obj);
    myModal.present();
  }

}
