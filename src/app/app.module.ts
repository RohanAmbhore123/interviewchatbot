import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { IonicStorageModule } from '@ionic/storage'; 

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { CandidateStorageServiceProvider } from '../providers/candidate-storage-service/candidate-storage-service';

import { ChatRoomPageModule } from '../pages/chat-room/chat-room.module';
import { CandidateListPageModule } from '../pages/candidate-list/candidate-list.module';
import { CandidateModalPageModule } from '../pages/candidate-modal/candidate-modal.module';

@NgModule({
  declarations: [
    MyApp,   
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ChatRoomPageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    CandidateListPageModule,
    CandidateModalPageModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,   
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CandidateStorageServiceProvider
  ]
})
export class AppModule {
  
}


