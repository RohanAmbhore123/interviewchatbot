export class CandidateData{
   
    public candidateData: Map<string,string>;

    constructor(){        
        this.candidateData = new Map<string,string>();        
    }

    addUserData(key:string,value:string){
        this.candidateData.set(key,value);
    }

    getUserData(key:string){
        return this.candidateData.get(key);
    }

    getAllData(){
        return this.candidateData;
    }

    getOpenings(){
        return ["Android developer ","Ionic developer ","iOS developer ","Solution Architecht ","IOT developer ","Web developer ","NodeJs developer ", " for which position you would like to apply for ?"]
    }

    reset(){
        this.candidateData = null;
    }

}