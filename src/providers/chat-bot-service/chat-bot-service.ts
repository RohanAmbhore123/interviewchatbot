import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { ENV } from '../../config/environment.dev';



/*
  Generated class for the ChatBotServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

// THIS SERVICE IS USED TO CALL THE BACKEND API'S

@Injectable()
export class ChatBotServiceProvider {
 
  constructor(public http: Http) {
    
  }

  getResponse(requestQuery){    // TO GET THE RESPONSE TO FOR CHATBOT USING OBSERVABLES

    let headers = new Headers();
     headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const options = new RequestOptions({ headers:headers}); 
    let data = JSON.stringify({ "request_text": requestQuery });
    
    return this.http.post(ENV.API_URL,data,options)
                    .map( res => res.json())
                    .catch(this.handleError);    
  }
  
  handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
