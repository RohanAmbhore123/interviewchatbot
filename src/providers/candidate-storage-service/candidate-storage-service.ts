import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';

/*
  Generated class for the CandidateStorageServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
// THIS SERVICE IS USED TO IMPLEMENT DATA STORAGE FOR THE PROJECT

@Injectable()
export class CandidateStorageServiceProvider {

  candidateStorageObserver:any;
  candidateStorageObserverService:any; 
  candidateData: any;
  candidateMap: Map<string,string>;

  constructor(public http: Http,public storage: Storage) {
    
    this.candidateStorageObserverService = Observable.create(observer => {
      this.candidateStorageObserver = observer;  
      this.candidateData = [];   
    });
    
  }

  setData(key,value){       //TO SET THE VALUE AND KEY FOR THE DATA
    this.storage.set(key,JSON.stringify(Array.from(value.candidateData.entries())));
    console.log(key);
    console.log(value.candidateData);
  }

  getValue(key):Observable<any>{      // TO GET THE VALUE FOR GIVEN KEY USING OBSERVABLES
    
    this.storage.get(key).then((data) => {
      console.log(JSON.parse(data));
      this.candidateStorageObserver.next(data);
    });

    return this.candidateStorageObserverService;
  }

  getAllData():Observable<any>{     //TO GET THE VALU FOR THE ENTIRE DATA USING OBSERVABLES

      if(this.storage){
      this.storage.forEach((value, key, index) => {
      console.log(value)
      this.candidateMap = new Map<string,string>(JSON.parse(value));
      console.log(this.candidateMap);
         if(this.candidateMap.get("firstName")!=null){
            
           let dataObject:any = {
             firstName:this.candidateMap.get("firstName"),
             lastName: this.candidateMap.get("lastName"),
             userData: this.candidateMap
           }
         
         this.candidateData.push(dataObject);
         this.candidateStorageObserver.next(this.candidateData);
       }
        
      });
      
      
      return this.candidateStorageObserverService;
      }
  }

}
