import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { ChatBotServiceProvider } from '../chat-bot-service/chat-bot-service';
import { CandidateData } from '../../utils/candidateData';
import { CandidateStorageServiceProvider } from '../candidate-storage-service/candidate-storage-service';

/*
  Generated class for the ChatRoomServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

// THIS SERVICE WILL HANDLE THE BUSINESS LOGIC FOR THE CHAT ROOM FEATURE

@Injectable()
export class ChatRoomServiceProvider {

  chatRoomServiceObserver: any;
  chatRoomService: any;
  candidateHasMap: CandidateData;
  candidateObjectJSON: any;
  key: string;

  constructor(public http: Http,private chatbotService : ChatBotServiceProvider, private candidateStorage : CandidateStorageServiceProvider) {

    this.candidateHasMap = new CandidateData();
    this.chatRoomService = Observable.create(observer => {
      this.chatRoomServiceObserver = observer;
    });
    this.candidateObjectJSON = [{}];
    this.key="";

  }

  prepareServiceCall(request_msg): Observable<any>{  //TO PREPARE A SERVICE AND HANDLE THE RESPONSE FOR THE PARTICULAR QUERY FROM BACKEND USING OBSERVABLES


     this.chatbotService.getResponse(request_msg).subscribe( (data) => {

        console.log(data.result);
        let chatBotResponse = data.result.fulfillment.messages; 
        let intent: string = data.result.metadata.intentName;        
        
        switch(intent){       // WHY WE USE SWITCH CASES? => TO HANDLE THE INTENT OF NLP ENGINE RESPONSE AND PARSE THE REQUIRED DATA
          case "emailId" :   
                let email = data.result.resolvedQuery.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);              
                this.candidateHasMap.addUserData("email",email[0]) 
                this.key = email[0];
                console.log(email[0]);
                break;

          case "firstName":
                let firstName = data.result.parameters.name;
                this.candidateHasMap.addUserData("firstName",firstName);
                break;

          case "lastName":
                let lastName = data.result.parameters.lastname;
                this.candidateHasMap.addUserData("lastName",lastName);
                break;

          case "location":
                let location = data.result.parameters.location;
                this.candidateHasMap.addUserData("location",location);
                break;

          case "relocate":
                let relocate = data.result.resolvedQuery;
                this.candidateHasMap.addUserData("relocate",relocate);
                break;

          case "skills":
                let skills = data.result.parameters.skills;
                this.candidateHasMap.addUserData("skills",skills);
                break;

          case "experience":
                let experience = data.result.parameters.experience;
                this.candidateHasMap.addUserData("experience",experience); 
                break;
 
          case "why":
                let why = data.result.parameters.why;
                this.candidateHasMap.addUserData("why you want to join",why);
                break;

          case "currentCompany":
                let companyName = data.result.parameters.companyName;
                this.candidateHasMap.addUserData("companyName",companyName);
                break;

          case "noticePeriod":
                let noticePeriod = data.result.resolvedQuery;
                this.candidateHasMap.addUserData("noticePeriod",noticePeriod);
                break;

          case "currentCTC":
                let currentCTC = data.result.resolvedQuery;
                this.candidateHasMap.addUserData("currentCTC",currentCTC);
                break;

          case "expectedCTC":
                let expectedCTC = data.result.resolvedQuery;
                this.candidateHasMap.addUserData("expectedCTC",expectedCTC);
                break;

          case "apply":
                let apply = data.result.resolvedQuery;
                this.candidateHasMap.addUserData("position",apply);
                break;

          case "end":
                console.log(this.candidateHasMap);               
                this.candidateStorage.setData(this.key,this.candidateHasMap);
                console.log(this.candidateHasMap.getAllData());
                chatBotResponse.push("end");                
                break;

          case "greetings":
                let speech = {
                  "speech" : this.candidateHasMap.getOpenings()
                }
                chatBotResponse.push(speech);
                break;

        }
        this.chatRoomServiceObserver.next(chatBotResponse);      
       
    },
     (error) => {
       console.log(error);
     });   

     return this.chatRoomService;
  }

}
