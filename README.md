# InterviewChatBot


## Purpose and Responsibilities

**Do not edit this section.**

The purpose of this document is to ensure standardization on GitHub repository README documents. Copy and paste the entire text of this document as is, into any projects that do not already have this README template.  **Do not place passwords in this document!**

- **Development Leads** are responsible for filling out and keeping this document up to date during the course of the project. 
- **Development Leads** are responsible for ensuring that developers adheare to this document, specifically when it comes to the Features NOT to use during testing section. 
- **Project Managers** are responsible for ensuring that the Development Lead has completed this document before project delivery. 
- **Support Developers** are responsible for adhearing to this document, specifically when it comes to the Features NOT to use during testing section.

## App Overview
The purpose of the InterviewBuddy(InterviewChatBot) is to help users to fill up their application form and submit it to recruiting panels for review.

## Features NOT to use during testing
N/A

## Build Instructions
To setup the development enviroment:
1) Install latest version of node
2) npm install -g cordova  //To install cordova
3) npm install -g ionic    //To install ionic


To build the app after cloning from this repo
1) npm install
2) ionic cordova platform remove android 
3) ionic cordova platform add android 
4) ionic cordova platform remove ios
5) ionic cordova platform add ios
6) ionic serve  //To see app in browser
7) ionic cordova run android or ionic cordova run ios   //To run the app in device or emulator


## Google Analytics
N/A

## Framework
 Ionic 3.6 and Angular 4.0

## Backend Environments / Recall Functionality
Set Backend URL at src/config/enviroment.dev.ts > API_URL

## Third party dependencies
 Api.ai NLP and ML engine

![picture](https://drive.google.com/file/d/0B0FzgmK_wG3pWG5lSHFRQlkxdU0/view)

